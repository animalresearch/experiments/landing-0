// eslint-disable-next-line
import React, {useState, useEffect} from 'react';
import logo from '../logo.svg';
import credentials from '../config/google-credentials.json';
// @ts-ignore next-line
import BotUI from 'botui-react';
import Bot from 'botui';

import {v4} from 'uuid';
import dialogflow, { protos } from '@google-cloud/dialogflow';
import {JWT} from 'google-auth-library';

import {useLocalStorage} from '../hooks/localStorage';
import {SessionsClient} from '@google-cloud/dialogflow/build/src/v2';

/**
 * Builds the bot com
 * @param {*} _props
 * @return {React.FunctionComponent<any>} Component with bot functionality
 */
const BotComponent: React.FunctionComponent<any> = (_props: any) => {
  const [botUI, setBotUI] = useState<Bot | undefined>();
  const [sessionId] = useLocalStorage('sessionId', v4());
  const [sessionClient, setSessionClient]:
  [SessionsClient | undefined, Function] = useState();
  const [sessionPath, setSessionPath] = useState({});
  const [started, setStarted] = useState(false);
  const [booleano, setBooleano] = useState(false);

  useEffect(() => {
    if (sessionId) {
      const authClient = new JWT({
        email: credentials.client_email,
        key: credentials.private_key,
        scopes: [
          'https://www.googleapis.com/auth/cloud-platform',
          'https://www.googleapis.com/auth/dialogflow',
        ],
      });

      const sClient = new dialogflow.v2.SessionsClient({
        fallback: true,
        // @ts-ignore next-line
        auth: authClient,
      });
      setSessionClient(sClient);
      setSessionPath(
          sClient
              .projectAgentSessionPath(credentials.project_id, `${sessionId}`),
      );
    }
  }, [sessionId]);

  useEffect(() => {
    /**
     * Awaits for a message
     * @param {*} bot el bot
     */
    async function msg(bot: Bot) {
      const msg = await bot.action.text({
        type: 'text',
        action: {
          placeholder: 'Ingresa tu texto',
          value: ''
        }
      });
      
      
      const result = await sessionClient?.detectIntent({ 
        session: `${sessionPath}`,
        queryInput: {
          text: {
            text: msg.value,
            languageCode: 'es-CO',
          },
        },
      });

      const response: protos.google.cloud.dialogflow.v2.IDetectIntentResponse | undefined = result?.[0];
      for (const message of response?.queryResult?.fulfillmentMessages ?? []) {
        const msgBox = await bot.message.add({
          type: 'text',
          loading: true,
          content: ''
        });
        
        setTimeout(
            async () => await bot?.message?.update(msgBox, {
              type: 'text',
              loading: false,
              content: message.text?.text?.[0] ?? '',
            }),
            600,
        );
      }

      setBooleano(!booleano);
    }
    
    if (sessionPath && botUI) {
      msg(botUI);
    }
  });

  return (
    <div className='landing-container'>
      {!started && <img alt='Owly' src={logo} className="owly-image" />}
      {!started ? (
        <a onClick={() => setStarted(true)} className='btn' href='#/'>
          Get Started
        </a>
      ) : (
        ''
      )}
      {started ? <BotUI ref={(bot: Bot) => setBotUI(bot)} /> : ''}
    </div>
  );
};

export default BotComponent;
