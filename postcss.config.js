const tailwindcss = require('tailwindcss');
module.exports = {
  autoprefixer: {
    flexbox: true,
  },
  plugins: [tailwindcss('./tailwind.js')],
};
