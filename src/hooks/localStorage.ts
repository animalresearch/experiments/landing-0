import {useState} from 'react';

/**
 * Local storage Hook
 * @param {string} key
 * @param {string} initialValue
 * @return {array} Hook with local storage
 */
export function useLocalStorage(key: string, initialValue: string) {
  const [storedValue, setStoredValue] = useState(
      window.localStorage.getItem(key) ?? initialValue,
  );

  const setValue = (value: string) => {
    window.localStorage.setItem(key, value);
    setStoredValue(value);
  };

  return [storedValue, setValue];
}
