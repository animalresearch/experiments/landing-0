// eslint-disable-next-line
import React from 'react';
import Bot from './components/bot';
import './App.css';

/**
 * Creates the app component
 * @return {JSX.Element} The component
 */
export default function App() {
  return (
    <section id='content' className="container mx-auto p-6">
      <h1 className="text-center text-6xl">Agent Owl</h1>
      <p className="text-center">
        Lorem ipsum dolor sit, amet consectetur adipisicing elit.
        Assumenda veniam vel illo neque sunt ab fugiat nesciunt,
        architecto reiciendis dolore animi fuga aspernatur quos odit
        ratione et qui libero similique!
      </p>
      <div className="bot-container max-w-sm rounded
          overflow-hidden shadow-lg mx-auto mt-4">
        <Bot />
      </div>
    </section>
  );
}
